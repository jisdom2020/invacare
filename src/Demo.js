import React, { useState, useEffect } from 'react';

function Demo() {
  const [searchTerm, setSearchTerm] = useState('');
  const [policyholderData, setPolicyholderData] = useState(null);
  const [selectedPolicyholder, setSelectedPolicyholder] = useState(null);

useEffect(() => {
  const fetchData = async () => {
    const params = new URLSearchParams();
    params.append('code', '1');

    const url = `http://localhost:3001/api/policyholders?${params.toString()}`;

    const response = await fetch(url, {
      method: 'GET',
    });

    const data = await response.json();
    return data;
  };

  fetchData().then((data) => {
    // console.log(data);
    setPolicyholderData(data);
  });
}, []);


const handleSearch = async () => {
  const response = await fetch(`http://localhost:3001/api/policyholders?code=${searchTerm}`, {
    method: 'GET',
  });
  const searchData = await response.json();
  setPolicyholderData(searchData);
};

  
  const handleNodeClick = (policyholder) => {
    setSelectedPolicyholder(policyholder);
  };
  
const renderPolicyholderTree = (data) => {
  if (!data) return null;

  return (
    <div>
      {data === policyholderData ? (
        <div>
          主節點 - code: {data.code} - name: {data.name}
        </div>
      ) : (
        <div onClick={() => handleNodeClick(data)}>
          子節點 - code: {data.code} - name: {data.name}
        </div>
      )}
      <div>
        {data.l && data.l.length > 0 && data.l.map((child) => (
          <div key={child.code}>{renderPolicyholderTree(child)}</div>
        ))}
      </div>
      <div>
        {data.r && data.r.length > 0 && data.r.map((child) => (
          <div key={child.code}>{renderPolicyholderTree(child)}</div>
        ))}
      </div>
    </div>
  );
};

  
  return (
    <div>
      <input
        type="text"
        placeholder="保戶編號"
        value={searchTerm}
        onChange={(e) => setSearchTerm(e.target.value)}
      />
      <button onClick={handleSearch}>搜索</button>
      {renderPolicyholderTree(selectedPolicyholder || policyholderData)}
    </div>
  );
}

export default Demo;
